<?php
/**
 * Elgg LESS compiler
 *
 * @package ElggLESS
 */

elgg_register_event_handler('init', 'system', 'less_init');

function less_init() {
	
	less_override_css();
	
	//make sure this runs after everyone else is done
	elgg_register_plugin_hook_handler('view', 'all', 'less_views', 999);
}

function less_views($hook, $type, $content, $params) {
	$view = $params['view'];
	$vars = $params['vars'];
	
	if (preg_match("/^css\//", $view) && !$vars['less_flag']) {
		global $CONFIG;
		if ($CONFIG->less->view[$view]) {
			$vars['less_flag'] = true;
			$content = elgg_view($CONFIG->less->view[$view], $vars);
		}
	}
	
	if (preg_match("/^less\//", $view) && $vars['less_flag']) {
		if (include_once(elgg_get_plugins_path() . 'less/vendors/lessphp/lessc.inc.php')) {
			$parser = new lessc();
			return $parser->parse($content);
		}
	}
	return $content;
}

function less_override_css() {
	$plugins = elgg_get_plugins('active');
	if ($plugins) {
		foreach ($plugins as $plugin) {
			$view_dir = elgg_get_plugins_path() . $plugin->getID() . "/views/";

			if (!is_dir($view_dir) || FALSE === ($handle = opendir($view_dir))) {
				continue;
			}

			while (FALSE !== ($view_type = readdir($handle))) {
				$view_type_dir = $view_dir . $view_type;

				if ('.' !== substr($view_type, 0, 1) && is_dir($view_type_dir)) {
					less_autoregister_views('', $view_type_dir, $view_dir, $view_type);
				}
			}
		}
	}
}

function less_autoregister_views($view_base, $folder, $base_location_path, $viewtype) {
	if (!empty($view_base)) {
		$view_base_new = $view_base . "/";
	} else {
		$view_base_new = "";
	}
	
	if ($handle = opendir($folder)) {
		while ($view = readdir($handle)) {
			if (!in_array($view, array('.', '..', '.svn', 'CVS')) && !is_dir($folder . "/" . $view)) {
				if (substr_count($view, ".php") > 0) {
					less_update_view($view_base_new . str_replace('.php', '', $view));
				}
			} else if (!in_array($view, array('.', '..', '.svn', 'CVS')) && is_dir($folder . "/" . $view)) {
				if ($view == 'css' || $view == 'less') {
					less_autoregister_views($view_base_new . $view, $folder . "/" . $view,
						$base_location_path, $viewtype);
				}
			}
		}
		return TRUE;
	}
	return FALSE;
}

function less_update_view($view) {
	global $CONFIG;
	
	if (substr($view, 0, 5) == "less/") {
		$css_view = substr($view, 5);
	} else {
		$css_view = $view;
	}

	if (!isset($CONFIG->less)) {
		$CONFIG->less = new stdClass;
	}

	if (!isset($CONFIG->less->view)) {
		$CONFIG->less->view = array($css_view => $view);

	} else {
		$CONFIG->less->view[$css_view] = $view;
	}
}
